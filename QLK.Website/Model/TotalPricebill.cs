﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLK.Website.Model
{
    public class TotalPricebill
    {
        public int ID { get; set; }
        public string IDProduct { get; set; }
        public int total { get; set; }
    }
}