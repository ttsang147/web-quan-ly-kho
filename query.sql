USE [QLK]
GO
/****** Object:  Table [Bill]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Bill](
	[BillID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[WarehouseID] [int] NULL,
	[BillName] [nvarchar](250) NULL,
	[Photo] [nvarchar](250) NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[BillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Category]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](250) NULL,
	[Description] [nvarchar](250) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DetailBill]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DetailBill](
	[DetailBillID] [int] IDENTITY(1,1) NOT NULL,
	[BillID] [int] NULL,
	[ProductID] [nvarchar](50) NULL,
	[Unit] [nvarchar](250) NULL,
	[Quantity] [int] NULL,
	[Note] [nvarchar](250) NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_DetailBill] PRIMARY KEY CLUSTERED 
(
	[DetailBillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Detailimportcoupon]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Detailimportcoupon](
	[DetailimportID] [int] IDENTITY(1,1) NOT NULL,
	[ImportID] [int] NULL,
	[Unit] [nvarchar](250) NULL,
	[Quantity] [int] NULL,
	[Price] [int] NOT NULL,
	[Location] [nvarchar](250) NULL,
	[ProductID] [nvarchar](50) NULL,
 CONSTRAINT [PK_Detailimportcoupon] PRIMARY KEY CLUSTERED 
(
	[DetailimportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Importcoupon]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Importcoupon](
	[ImportID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[WarehouseID] [int] NULL,
	[ImportName] [nvarchar](250) NULL,
	[Photo] [nvarchar](250) NULL,
 CONSTRAINT [PK_Importcoupon] PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Product]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Product](
	[ProductID] [nvarchar](50) NOT NULL,
	[CategoryID] [int] NULL,
	[SupplierID] [int] NULL,
	[ProductName] [nvarchar](250) NULL,
	[Unit] [nvarchar](250) NULL,
	[Photo] [nvarchar](250) NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Supplier]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Supplier](
	[SupplierID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nvarchar](250) NULL,
	[ContactName] [nvarchar](250) NULL,
	[Adderss] [nvarchar](250) NULL,
	[City] [nvarchar](250) NULL,
	[Zipcode] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Phone] [nvarchar](250) NULL,
	[Note] [nvarchar](250) NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [SystemDatabase]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SystemDatabase](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
 CONSTRAINT [PK_SystemDatabase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Uploadfile]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Uploadfile](
	[IDPath] [int] IDENTITY(1,1) NOT NULL,
	[Path] [nvarchar](250) NULL,
	[ID] [int] NULL,
 CONSTRAINT [PK_Uploadfile] PRIMARY KEY CLUSTERED 
(
	[IDPath] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [UploadPhotoBill]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UploadPhotoBill](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdBill] [int] NULL,
	[Path] [nvarchar](250) NULL,
 CONSTRAINT [PK_UploadPhotoBill] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [UploadPhotoProduct]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UploadPhotoProduct](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDProduct] [nvarchar](50) NULL,
	[Path] [nvarchar](250) NULL,
 CONSTRAINT [PK_UploadPhotoProduct] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Warehouse]    Script Date: 12/18/2023 4:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Warehouse](
	[WarehouseID] [int] IDENTITY(1,1) NOT NULL,
	[WarehouseName] [nvarchar](50) NULL,
	[Soluong] [float] NULL,
 CONSTRAINT [PK_Warehouse] PRIMARY KEY CLUSTERED 
(
	[WarehouseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [Bill] ON 

INSERT [Bill] ([BillID], [Date], [WarehouseID], [BillName], [Photo]) VALUES (1158, CAST(N'2023-12-18' AS Date), 1048, N'Phiếu xuất 1', NULL)
SET IDENTITY_INSERT [Bill] OFF
SET IDENTITY_INSERT [Supplier] ON 

INSERT [Supplier] ([SupplierID], [SupplierName], [ContactName], [Adderss], [City], [Zipcode], [Country], [Phone], [Note]) VALUES (1030, N'Viettel post', N'viettel', N'huế', N'huế', N'505050', N'Việt Nam', N'18001008', N'rẻerds')
SET IDENTITY_INSERT [Supplier] OFF
SET IDENTITY_INSERT [SystemDatabase] ON 

INSERT [SystemDatabase] ([Id], [Username], [Password], [FullName]) VALUES (1, N'sang', N'202cb962ac59075b964b07152d234b70', N'truong thanh sang')
INSERT [SystemDatabase] ([Id], [Username], [Password], [FullName]) VALUES (2, N'sangtruong', N'202cb962ac59075b964b07152d234b70', N'truong thanh sang')
INSERT [SystemDatabase] ([Id], [Username], [Password], [FullName]) VALUES (3, N's', N'202cb962ac59075b964b07152d234b70', N'truong thanh sang')
INSERT [SystemDatabase] ([Id], [Username], [Password], [FullName]) VALUES (4, N'123', N'202cb962ac59075b964b07152d234b70', N'truong thanh sang')
SET IDENTITY_INSERT [SystemDatabase] OFF
SET IDENTITY_INSERT [Warehouse] ON 

INSERT [Warehouse] ([WarehouseID], [WarehouseName], [Soluong]) VALUES (1048, N'Kho áo quần', 1000)
INSERT [Warehouse] ([WarehouseID], [WarehouseName], [Soluong]) VALUES (1049, N'kho 1', 100000)
INSERT [Warehouse] ([WarehouseID], [WarehouseName], [Soluong]) VALUES (1050, N'kho 3', 100000)
SET IDENTITY_INSERT [Warehouse] OFF
ALTER TABLE [Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Warehouse] FOREIGN KEY([WarehouseID])
REFERENCES [Warehouse] ([WarehouseID])
GO
ALTER TABLE [Bill] CHECK CONSTRAINT [FK_Bill_Warehouse]
GO
ALTER TABLE [DetailBill]  WITH CHECK ADD  CONSTRAINT [FK_DetailBill_Bill] FOREIGN KEY([BillID])
REFERENCES [Bill] ([BillID])
GO
ALTER TABLE [DetailBill] CHECK CONSTRAINT [FK_DetailBill_Bill]
GO
ALTER TABLE [DetailBill]  WITH CHECK ADD  CONSTRAINT [FK_DetailBill_Product] FOREIGN KEY([ProductID])
REFERENCES [Product] ([ProductID])
GO
ALTER TABLE [DetailBill] CHECK CONSTRAINT [FK_DetailBill_Product]
GO
ALTER TABLE [Detailimportcoupon]  WITH CHECK ADD  CONSTRAINT [FK_Detailimportcoupon_Importcoupon] FOREIGN KEY([ImportID])
REFERENCES [Importcoupon] ([ImportID])
GO
ALTER TABLE [Detailimportcoupon] CHECK CONSTRAINT [FK_Detailimportcoupon_Importcoupon]
GO
ALTER TABLE [Detailimportcoupon]  WITH CHECK ADD  CONSTRAINT [FK_Detailimportcoupon_Product] FOREIGN KEY([ProductID])
REFERENCES [Product] ([ProductID])
GO
ALTER TABLE [Detailimportcoupon] CHECK CONSTRAINT [FK_Detailimportcoupon_Product]
GO
ALTER TABLE [Importcoupon]  WITH CHECK ADD  CONSTRAINT [FK_Importcoupon_Warehouse] FOREIGN KEY([WarehouseID])
REFERENCES [Warehouse] ([WarehouseID])
GO
ALTER TABLE [Importcoupon] CHECK CONSTRAINT [FK_Importcoupon_Warehouse]
GO
ALTER TABLE [Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([CategoryID])
REFERENCES [Category] ([CategoryID])
GO
ALTER TABLE [Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Supplier] FOREIGN KEY([SupplierID])
REFERENCES [Supplier] ([SupplierID])
GO
ALTER TABLE [Product] CHECK CONSTRAINT [FK_Product_Supplier]
GO
